#include <stdio.h>
#include <math.h>
#include <random>
#include <time.h>
#pragma warning(disable:4996)

void main()
{
	int temp;
	int nTime =1000;
	int lindex, rindex;
	double J = 5;
	int n = 10;
	int atoms [10];
	int atoms_new[10];
	int histoValues[23];
	int sumOfEnergy =0;
	int current, lside, rside;
	double val;
	int ourRandom;
	double gamma = 1 /float( n);
	double probabilityOfFlip;

	FILE * myPointer;
	myPointer = fopen("ENERGY.dat", "w");
	fclose(myPointer);
	
	for (int i = 0; i< n; ++i)
	{
		//ourRandom = rand();
		//if (ourRandom%2 == 0)
		//{
			//atoms[i] = 1;
		//}
		//else
		//{
		//	atoms[i] = -1;
	//	}
		atoms[i] = -1;
	}

	for (int x = 0; x < 23; ++x)
	{
		histoValues[x] = 0;
	}

	for (int time = 0; time < nTime; ++time)
	{
		for (int updateLoop = 0; updateLoop < n; ++updateLoop)
		{
			val = (double(rand()) / double(RAND_MAX));
			current = atoms[updateLoop];	
			lindex = updateLoop - 1;
			rindex = updateLoop + 1;
			if (lindex < 0)
			{
				lindex = n - 1;
			}
			if (rindex = n)
			{
				rindex = 0;
			}

			lside = atoms[lindex];
			current = atoms[updateLoop];
			rside = atoms[rindex];
			atoms_new[updateLoop] = current;
			probabilityOfFlip = 0.5*gamma*(1 + double(current)*tanh(J*lside + J*rside));
			printf("\nProb flip: %.5f", probabilityOfFlip);
			printf("\nRandom Val: %.5f", val);
			printf("\nValue of Current State: %d", atoms[updateLoop]);
			if (val < probabilityOfFlip)
			{
				atoms_new[updateLoop] = -1 * current;
			}
			printf("\nNew State: %d", atoms_new[updateLoop]);
		}// end update loop

		for (int p = 0; p < n - 1; ++p)
		{
			temp = atoms_new[p];
			atoms[p] = temp;
		}

		for (int j = 0; j < n-1; ++j)
		{
			sumOfEnergy = sumOfEnergy - J*(atoms[j] * atoms[j + 1]);
		}

		sumOfEnergy = sumOfEnergy + (atoms[n-1] *atoms[0]);
		//printf("\nEnergy: %d", sumOfEnergy);
		myPointer = fopen("ENERGY.dat", "a");
		fprintf(myPointer, "\n%d", sumOfEnergy);
		fclose(myPointer);
		switch (sumOfEnergy)
		{
		case -11:
			histoValues[0] = histoValues[0] + 1;
			break;
		case -10:
			histoValues[1] = histoValues[1] + 1;
			break;
		case -9:
			histoValues[2] = histoValues[2] + 1;
			break;
		case -8:
			histoValues[3] = histoValues[3] + 1;
			break;
		case -7:
			histoValues[4] = histoValues[4] + 1;
			break;
		case -6:
			histoValues[5] = histoValues[5] + 1;
			break;
		case -5:
			histoValues[6] = histoValues[6] + 1;
			break;
		case -4:
			histoValues[7] = histoValues[7] + 1;
			break;
		case -3:
			histoValues[8] = histoValues[8] + 1;
			break;
		case -2:
			histoValues[9] = histoValues[9] + 1;
			break;
		case -1:
			histoValues[10] = histoValues[10] + 1;
			break;
		case 0:
			histoValues[11] = histoValues[11] + 1;
			break;
		case 1:
			histoValues[12] = histoValues[12] + 1;
			break;
		case 2:
			histoValues[13] = histoValues[13] + 1;
			break;
		case 3:
			histoValues[14] = histoValues[14] + 1;
			break;
		case 4:
			histoValues[15] = histoValues[15] + 1;
			break;
		case 5:
			histoValues[16] = histoValues[16] + 1;
			break;
		case 6:
			histoValues[17] = histoValues[17] + 1;
			break;
		case 7:
			histoValues[18] = histoValues[18] + 1;
			break;
		case 8:
			histoValues[19] = histoValues[19] + 1;
			break;
		case 9:
			histoValues[20] = histoValues[20] + 1;
			break;
		case 10:
			histoValues[21] = histoValues[21] + 1;
			break;
		case 11:
			histoValues[22] = histoValues[22] + 1;
			break;
		default:
			break;
		}// end switch

		
		sumOfEnergy = 0;

	}// end time loop
	int bucket = -11;
	for (int g = 0; g < 23; g = g + 1)
	{
		printf("\nBucket %d: %d", bucket, histoValues[g]);
		bucket = bucket + 1;
	}
	getchar();
}//end Program
//*/